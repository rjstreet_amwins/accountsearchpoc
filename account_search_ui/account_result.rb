class AccountResult
  def initialize(name = '', alternateName = '', address = '', group = '', city = '')  
    @name = name
    @address = address
    @group = group
    @city = city
    @alternateName = alternateName
  end 
  attr_accessor :name, :address, :group, :city, :alternateName
end