require 'sinatra'
require 'haml'
require 'sinatra/partial'
require 'uri'
require 'json'
require 'net/https'

load "account_result.rb"

helpers do
  def run_ads(text="Get Sinatra today!")
    text
  end

end

get "/search" do
	results = Array.new
	if( params['account_state'] == '' )
		uri = URI.parse("http://localhost:43977/api/account/search?searchText=" + params['account_name'])
	else
		uri = URI.parse("http://localhost:43977/api/account/search?searchText=" + params['account_name'] + "&stateCode=" + params['account_state'])		
	end
	http = Net::HTTP.new(uri.host, uri.port)
	request = Net::HTTP::Get.new(uri.request_uri)

	res = http.request(request)
	response = JSON.parse(res.body)
	puts response
	response['results'].each do | result |
		results << AccountResult.new( result['account_name'], result['account_alternate_name'], result['state_cd'], result['class_description'], result['city'])
	end

	partial(:result, {locals: {items: results}, layout: false})
end

get "/" do
  haml :index
end

__END__