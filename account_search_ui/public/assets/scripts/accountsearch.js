function perform_search() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
    	document.getElementById("results_grid").innerHTML = xhttp.responseText;
    }
  };
  xhttp.open("GET", "http://localhost:4567/search?account_name=" + document.getElementById("account_name").value + "&account_state=" + document.getElementById("account_state").value, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("fname=Henry&lname=Ford");
}