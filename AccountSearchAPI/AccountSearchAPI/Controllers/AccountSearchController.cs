﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using Sphinx.Client.Commands.Attributes;
using System.Web;

namespace AccountSearchAPI.Controllers
{
    public class SearchInput
    {
        public String searchText { get; set; }
        public String stateCode { get; set; }
    }

    [RoutePrefix("api/account")]
    public class AccountSearchController : ApiController
    {

        [Route("search")]
        public HttpResponseMessage GetSearch([FromUri]SearchInput input)
        {
            String builder = @"{""results"": [ ";
            using (ConnectionBase connection = new PersistentTcpConnection("localhost", 9312))
            {
                // Create new search query object and pass query text as argument
                SearchQuery searchQuery;
                if( input.stateCode == null )
                   searchQuery = new SearchQuery("@(account_name,account_alternate_name,state_cd,city) " + input.searchText);
                else
                    searchQuery = new SearchQuery("@(account_name,account_alternate_name,state_cd,city) " + input.searchText + " @state_cd " + input.stateCode);
                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;
                // Add Sphinx index name to list
                searchQuery.Indexes.Add("account_search");
                // Setup attribute filter
                //DateTime[] datesToExclude = new DateTime[] {DateTime.ParseExact("2005-05-05 20:07:09", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture)};
                //searchQuery.AttributeFilters.Add("date_added", datesToExclude, true);
                //searchQuery.AttributeFilters.Add("date_added", new DateTime(2000, 1, 1), new DateTime(2015, 12, 31), false);
                // Set amount of matches will be returned to client 
                searchQuery.Limit = 200;
                
                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);
                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);
                // Execute command on server and obtain results
                searchCommand.Execute();

                // Print results
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        builder = builder + "{";
                        foreach (var attr in match.AttributesValues)
                        {
                            builder = builder + @"""" + HttpUtility.JavaScriptStringEncode(attr.Name) + @""": """ + HttpUtility.JavaScriptStringEncode((string)attr.GetValue()) + @""",";
                        }
                        builder = builder.Remove(builder.Length - 1);
                        builder = builder + @"},";
                    }
                    builder = builder.Remove(builder.Length-1);
                    builder = builder + "],";
                    builder = builder + @"""time_elapsed"": """ + result.ElapsedTime.TotalMilliseconds + @""",";
                    builder = builder + @"""total_found"": """ + result.TotalFound + @""",";
                    builder = builder + @"""total_returned"": """ + result.Count + @"""}";
                }
            }

            return new HttpResponseMessage() { Content = new StringContent(builder, System.Text.Encoding.UTF8, "text/html") };
        }
    }
}
