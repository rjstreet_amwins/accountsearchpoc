Sphinx-based account search PoC.  Requires:
  * Sphinx and the creation of a new index (account_search as specified in the sphinx.conf file)
  * Ruby with Sinatra installed (for the UI)
  * VStudio with WebAPI2